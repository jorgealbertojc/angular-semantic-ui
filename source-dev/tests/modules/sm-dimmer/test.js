// import Calculator from "helpers/modules"


(() => {

    'use strict'


    describe( 'unit testing sm-dimmer directive', () => {


        let $compile
        let $scope
        let directiveElement
        let directiveCtrl
        let isolatedScope

        beforeEach( angular.mock.module('smDimmer') )

        beforeEach( () => {

            angular.mock.inject( ( _$compile_, $rootScope ) => {
                $compile = _$compile_
                $scope = $rootScope.$new()
            })
            directiveElement = getDirectiveElement()
            isolatedScope = directiveElement.isolateScope()
            directiveCtrl = directiveElement.controller('smDimmer')

        })


        const getDirectiveElement = () => {

            let elementHTML =   `<div 
                                    sm-dimmer 
                                    display = 'dimmerObj.display'
                                    dimmer-class = 'dimmerObj.dimmerClass'
                                    hover = 'dimmerObj.hover'
                                >            
                                    <dimmer-pane>
                                        <p>Test</p>
                                        <p>Test</p>
                                    </dimmer-pane>
                                    <dimmer-template>
                                        <div>
                                            <h2 class="ui inverted header">
                                                Dimmer content from template url
                                            <h2>
                                        </div>
                                    </dimmer-template>
                                </div>`
            
            let element  = angular.element(elementHTML)
            let compiledElement = $compile(element)($scope)
            $scope.dimmerObj = { 
                display: 'hide',
                dimmerClass: 'page',
                hover: false
            }
            $scope.$digest()
            return compiledElement

        }


        it( 'should has dimmer inside', () => {
            // console.log(directiveElement[0])
            let dimmer = directiveElement.find('.dimmer')
            expect( dimmer.length ).toBe(1)
        })


        it( 'should has isolated scopes', () => {
            expect( isolatedScope.display ).toBe('hide')
            expect( isolatedScope.dimmerClass ).toBe('page')
            expect( isolatedScope.hover ).toBe(false)
        })


        it( 'should change isolated scope', () => {
            $scope.dimmerObj.display = 'show'
            $scope.dimmerObj.dimmerClass = ''
            $scope.dimmerObj.hover = true
            $scope.$digest()
            expect( isolatedScope.display ).toBe('show')
            expect( isolatedScope.dimmerClass ).toBe('')
            expect( isolatedScope.hover ).toBe(true)
        })


        it( 'should make dimmer visible', () => {

            spyOn( isolatedScope, 'changeView' )
            $scope.dimmerObj.display = 'show'
            $scope.$digest()
            expect( isolatedScope.changeView.calls.count() ).toBe(1)
            
        })


    })


})()