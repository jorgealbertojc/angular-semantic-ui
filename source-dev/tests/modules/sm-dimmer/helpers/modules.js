// module.exports  Calculator


(() => {

    'use strict'

    
    class Calculator {

        sum( op1, op2 ) {
            return op1 + op2
        }
        
        subtract ( op1, op2 ) {
            return op1 - op2
        }

        divide ( x, y ) {
            return (y === 0) ? 0 : x / y
        }

    }


})()


// // //------ lib.js ------ 
// // export const sqrt = Math.sqrt; 
// // export function square(x) { return x * x; } 
// // export function diag(x, y) { return sqrt(square(x) + square(y)); 
// // } 
// //------ main.js ------ 
// import { square, diag } from 'lib'; 
// console.log(square(11)); // 121 console.log(diag(4, 3)); // 5