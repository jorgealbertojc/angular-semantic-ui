// import Calculator from "helpers/modules"


(() => {

    'use strict'



    const createSpyObj = ( constr, name ) => {
        let keys = []
        let array = Object.getPrototypeOf(constr)
        console.log(array)
        for( let key in constr ) {
            keys.push( key )
        }
        return keys.length > 0 ? jasmine.createSpyObj( name || "mock", keys ) : {}
    }


    describe( 'unit testing sm-dimmer component', () => {

        let controller
        let controllerSpy
        let scope
        let dimmerMock

        beforeEach( angular.mock.module('smDimmerRoute') )

        beforeEach( () => {
            angular.mock.inject( ( $rootScope, $componentController ) => {
                scope = $rootScope.$new()
                controller = $componentController( 'smDimmerComponentRoute', {$scope: scope} )
            })
        })

        beforeEach( () => {
            controllerSpy = createSpyObj( controller )
            dimmerMock = {
                display: 'hide',
                dimmerClass: '',
                hover: false,
                state: '',
                opacity: 0.5,
                functions: {
                    setOpacity: ( value ) => {},
                    getDimmer: () => { 
                        return {
                            outerHTML: '<div class="dimmer"></div>'
                        }
                    },
                    getDuration: () => {},
                    hasDimmer: () => {},
                    isActive: () => {},
                    isDisabled: () => {},
                    isAnimating: () => {},
                    isDimmer: () => {},
                    isDimmable: () => {},
                    isPageDimmer: () => {},
                    setActive: () => {},
                    setDimmable: () => {},
                    setDimmed: () => {},
                    setPageDimmer: () => {},
                    setDisabled: () => {}
                }
            }
        })



        it( 'should check initial settings', () => {

            spyOn( controller, 'getDimmer' )
            expect( controller.getDimmer.calls.count() ).toBe(0)

        })


        it( 'should create spies for each exposed method in the controller', () => {

            expect( controllerSpy.getDimmer ).toBeDefined()
            expect( controllerSpy.getDuration ).toBeDefined()
            expect( controllerSpy.hasDimmer ).toBeDefined()
            expect( controllerSpy.isActive ).toBeDefined()
            expect( controllerSpy.isDisabled ).toBeDefined()
            expect( controllerSpy.isAnimating ).toBeDefined()
            expect( controllerSpy.isDimmer ).toBeDefined()
            expect( controllerSpy.isDimmable ).toBeDefined()
            expect( controllerSpy.isPageDimmer ).toBeDefined()
            expect( controllerSpy.setActive ).toBeDefined()
            expect( controllerSpy.setDimmable ).toBeDefined()
            expect( controllerSpy.setDimmed ).toBeDefined()
            expect( controllerSpy.setPageDimmer ).toBeDefined()
            expect( controllerSpy.setDisabled ).toBeDefined()
            expect( controllerSpy.displayDimmer ).toBeDefined()
            expect( controllerSpy.setDimmerState ).toBeDefined()


        })


        it( 'should check if the spies has been called', () => {

            controllerSpy.getDimmer()
            controllerSpy.getDuration()
            controllerSpy.hasDimmer()
            controllerSpy.isActive()
            controllerSpy.isDisabled()
            controllerSpy.isAnimating()
            controllerSpy.isDimmer()
            controllerSpy.isDimmable()
            controllerSpy.isPageDimmer()
            controllerSpy.setActive()
            controllerSpy.setDimmable()
            controllerSpy.setDimmed()
            controllerSpy.setPageDimmer()
            controllerSpy.setDisabled()
            controllerSpy.displayDimmer()
            controllerSpy.setDimmerState()

            expect( controllerSpy.getDimmer ).toHaveBeenCalled()
            expect( controllerSpy.getDuration ).toHaveBeenCalled()
            expect( controllerSpy.hasDimmer ).toHaveBeenCalled()
            expect( controllerSpy.isActive ).toHaveBeenCalled()
            expect( controllerSpy.isDisabled ).toHaveBeenCalled()
            expect( controllerSpy.isAnimating ).toHaveBeenCalled()
            expect( controllerSpy.isDimmer ).toHaveBeenCalled()
            expect( controllerSpy.isDimmable ).toHaveBeenCalled()
            expect( controllerSpy.isPageDimmer ).toHaveBeenCalled()
            expect( controllerSpy.setActive ).toHaveBeenCalled()
            expect( controllerSpy.setDimmable ).toHaveBeenCalled()
            expect( controllerSpy.setDimmed ).toHaveBeenCalled()
            expect( controllerSpy.setPageDimmer ).toHaveBeenCalled()
            expect( controllerSpy.setDisabled ).toHaveBeenCalled()
            expect( controllerSpy.displayDimmer ).toHaveBeenCalled()
            expect( controllerSpy.setDimmerState ).toHaveBeenCalled()


        })


        it( 'should check conditionals inside getDimmer function ', () => {

            dimmerMock.dimmer = undefined
            spyOn( controller, 'getDimmer' ).and.callThrough()
            controller.getDimmer( dimmerMock )
            expect( controller.getDimmer ).toHaveBeenCalledWith( dimmerMock )
            expect( dimmerMock.dimmer ).toBe( dimmerMock.functions.getDimmer().outerHTML )

            dimmerMock.functions.getDimmer = undefined
            dimmerMock.dimmer = undefined
            controller.getDimmer( dimmerMock )
            expect( controller.getDimmer ).toHaveBeenCalledWith( dimmerMock )
            expect( dimmerMock.dimmer ).toBeUndefined()


        })

    })


})()