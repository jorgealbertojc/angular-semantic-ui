

(() => {

    'use strict'

    const smEmbendComponentRoute = {
        templateUrl: 'modules/sm-embed/docs.html'
    }


    angular
    .module( 'smEmbedRoute', ['smEmbed'] )
    .component( 'smEmbedComponentRoute', smEmbendComponentRoute )


})()
