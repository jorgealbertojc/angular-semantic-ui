

(() => {
    'use strict'


    const SmEmbedCtrl = function ( ) {

        const ctrl = this


        const setup = () => {
            setupVars()
        }


        const setupVars = () => {

            ctrl.configEmbed1 = {
                source: "vimeo",
                id: "125292332",
                placeholder: "http://semantic-ui.com/images/vimeo-example.jpg"
            }

            ctrl.behavior1 = {
                input: "get id"
            }


            ctrl.behavior2 = "hide"

        }


        setup()


    }


    angular
    .module('angularSemanticUI')
    .controller( 'SmEmbedCtrl', SmEmbedCtrl )


})()
