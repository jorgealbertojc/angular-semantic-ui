

(() => {
    'use strict'


    const link = ( $scope, element, attrs, ngModel, transclude ) => {
        let $element = null;


        const setup = () => {
            getElementEmbed()
            setupEmbed()
            setBehavior()
        }


        const getElementEmbed = () => {
            $element = $( element )
        }


        const setupEmbed = () => {

            $scope.config = $scope.config || {}

            $element
                .embed( $scope.config )
        }


        const setBehavior = () => {

            if ( angular.isObject( $scope.behavior ) ) {
                $scope.behavior.output = $element.embed( $scope.behavior.input )
            } else if ( angular.isString( $scope.behavior ) ) {
                $element.embed( $scope.behavior )
            }

        }


        setup()


    }


    const smEmbed = () => {
        return {
            restrict: 'A',
            scope: {
                config: "=?",
                behavior: "=?"
            },
            link: link
        }
    }


    angular
    .module( 'smEmbed', [] )
    .directive( 'smEmbed', smEmbed )


})()
