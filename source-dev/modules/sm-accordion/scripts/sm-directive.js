

(() => {
    'use strict'


    const link = ( $scope, element, attrs, ngModel, transclude ) => {


        const setup = () => {
            setupAccordion()
        }


        const setupAccordion = () => {
            if( $scope.selectorClass &&  $scope.selectorClass !== '' ){
                $( element )
                .accordion({
                    selector: {
                        trigger: $scope.selectorClass
                    }
                })
            }
            else{
                $( element )
                .accordion({
                    selector: {
                        trigger: '.title .icon'
                    }
                })
            }
        }


        setup()


    }


    const smAccordion = () => {

        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            scope:{
                selectorClass : "@"
            },
            template: '<div class="ui accordion">' +
                '<ng-transclude>  </ng-transclude>' +
            '</div>',
            link: link
        }

    }


    angular
    .module( 'smAccordion', [] )
    .directive( 'smAccordion', smAccordion )


})()
