

(() => {

    'use strict'

    const smAccordionComponentRoute = {
        templateUrl: 'modules/sm-accordion/docs.html'
    }


    angular
    .module( 'smAccordionRoute', ['smAccordion'] )
    .component( 'smAccordionComponentRoute', smAccordionComponentRoute )


})()
