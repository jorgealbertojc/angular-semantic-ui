

(() => {
    'use strict'


    const link = ( $scope, element, attrs, ngModel, transclude ) => {


        const setup = () => {
            setState()
            setFunctions()
        }


        const setFunctions = () => {
            if( $scope.ngFunctions ){
                $( element )
                .checkbox()
                .first().checkbox( $scope.ngFunctions )
            }
        }


        const setState = () => {

            if( $scope.ngState ){
                $( element ).checkbox( 'set ' + $scope.ngState )
            }
            $scope.$watch( 'ngState', function( _new, _old ){
                if( _old !== _new ){
                    $( element ).checkbox( 'set ' + _new )
                }
            });
        }


        setup()


    }


    const smCheckbox = () => {

        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            scope:{
                ngState: "=",
                ngFunctions: "=",
            },
            template: '<div class="ui checkbox">' +
                '<input type="checkbox">' +
                '<label> <ng-transclude>  1 </ng-transclude> </label>' +
            '</div>',
            link: link
        }

    }


    angular
    .module( 'smCheckbox', [] )
    .directive( 'smCheckbox', smCheckbox )


})()
