

(() => {

    'use strict'

    const smCheckboxComponentRoute = {
        templateUrl: 'modules/sm-checkbox/docs.html'
    }


    angular
    .module( 'smCheckboxRoute', ['smCheckbox'] )
    .component( 'smCheckboxComponentRoute', smCheckboxComponentRoute )


})()
