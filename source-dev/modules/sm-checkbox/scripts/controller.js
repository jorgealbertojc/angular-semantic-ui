

(() => {
    'use strict';


    const SmCheckboxCtrl = function ( ) {

        const ctrl = this


        const setup = () => {
            setupVars()
        }


        const setupVars = () => {
            ctrl.checkbox =  {};
            ctrl.checkbox.checkbox1 = {
                txt : "option 1",
                disable: false,
                state: "checked"
            }
            ctrl.checkbox.checkbox2 = ctrl.checkbox3 = {
                txt: "option 2",
                determinate : "indeterminate",
                state: "checked"
            }
            ctrl.checkbox.checkbox3 = {
                txt: "option 3",
                determinate : "indeterminate",
                state: "unchecked"
            }

            ctrl.callbackFunctions = {
                onChecked: function() {
                    console.log('onChecked called');
                },
                onUnchecked: function() {
                    console.log('onUnchecked called');
                },
                onEnable: function() {
                    console.log('onEnable called');
                },
                onDisable: function() {
                    console.log('onDisable called');
                },
                onDeterminate: function() {
                    console.log('onDeterminate called');
                },
                onIndeterminate: function() {
                    console.log('onIndeterminate called');
                },
                onChange: function() {
                    console.log('onChange called');
                }
            }
        }


        ctrl.check = () =>{
            var checkbox
            for( checkbox in ctrl.checkbox ){
                ctrl.checkbox[ checkbox ].state = 'checked'
            }
        }


        ctrl.uncheck = () =>{
            var checkbox
            for( checkbox in ctrl.checkbox ){
                ctrl.checkbox[ checkbox ].state = 'unchecked'
            }
        }


        ctrl.disabled = () =>{
            ctrl.checkbox.checkbox1.state = 'disabled';
        }


        ctrl.enable = () => {
            ctrl.checkbox.checkbox1.state = 'enabled';
        }


        ctrl.determinate = () =>{
            ctrl.checkbox.checkbox3.state = "determinate"
        }


        ctrl.indeterminate = () =>{
            ctrl.checkbox.checkbox3.state = "indeterminate"
        }


        setup()


    }


    angular
    .module('angularSemanticUI')
    .controller( 'SmCheckboxCtrl', SmCheckboxCtrl );


})()
