

(() => {
    'use strict';


    const DimmerDocsCtrl = function ( $scope ) {
        

        const ctrl = this


        const setup = () => {
            setupVars()
            setDimmerFunctions()
        }


        const setupVars = () => {
            ctrl.dimmer1 = {
                display: 'hide',
                dimmerClass: ''
            }
            ctrl.dimmer2 = {
                display: 'hide',
                dimmerClass: 'inverted'
            }
            ctrl.dimmer3 = {
                dimmerClass: 'simple'
            }
            ctrl.dimmer4 = {
                state: 'active'
            }
            ctrl.dimmer5 = {
                state: 'disabled'
            }
            ctrl.dimmer6 = {
                display: 'hide'
            }
            ctrl.dimmer7 = {
                display: 'hide',
                hover: false
            }
            ctrl.dimmer8 = {
                display: 'hide',
                dimmerClass: 'page',
                hover: false
            }
            ctrl.dimmer9 = {
                display: '',
                dimmerClass: '',
                hover: true,
                state: ''
            }
            ctrl.dimmer10 = {
                display: 'hide',
                dimmerClass: '',
                hover: false,
                state: '',
                opacity: 0.5,
                functions: {
                    setOpacity: ( value ) => {},
                    getDimmer: () => {},
                    getDuration: () => {},
                    hasDimmer: () => {},
                    isActive: () => {},
                    isDisabled: () => {},
                    isAnimating: () => {},
                    isDimmer: () => {},
                    isDimmable: () => {},
                    isPageDimmer: () => {},
                    setActive: () => {},
                    setDimmable: () => {},
                    setDimmed: () => {},
                    setPageDimmer: () => {},
                    setDisabled: () => {}
                }
            }
        }


        const setDimmerFunctions = () => {
            
        }


        // FUNCTIONS

        ctrl.getDimmer = ( dimmer ) => {
            if( dimmer.functions.getDimmer ) {
                let dimmerDom = dimmer.functions.getDimmer().outerHTML 
                dimmer.dimmer = dimmerDom.replace( /  /g, '' )
                // dimmer.dimmer = dimmer.dimmer.replace( /\n/g, '' )
                // dimmer.dimmer = dimmer.dimmer.replace( />/g, '>\n' )
            }
        }


        ctrl.getDuration = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.getDuration ) {
                dimmer.duration = dimmer.functions.getDuration()
            }
        }


        ctrl.hasDimmer = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.hasDimmer ) {
                dimmer.hasDimmer = dimmer.functions.hasDimmer()
            }
        }


        ctrl.isActive = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.isActive ) {
                dimmer.isActive = dimmer.functions.isActive()
            }
        }


        ctrl.isDisabled = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.isDisabled ) {
                dimmer.isDisabled = dimmer.functions.isDisabled()
            }
        }


        ctrl.isAnimating = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.isAnimating ) {
                dimmer.isAnimating = dimmer.functions.isAnimating()
            }
        }


        ctrl.isDimmer = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.isDimmer ) {
                dimmer.isDimmer = dimmer.functions.isDimmer()
            }
        }


        ctrl.isDimmable = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.isDimmable ) {
                dimmer.isDimmable = dimmer.functions.isDimmable()
            }
        }


        ctrl.isPageDimmer = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.isPageDimmer ) {
                dimmer.isPageDimmer = dimmer.functions.isPageDimmer()
            }
        }

        // SETS
        ctrl.setActive = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.setActive ) {
                dimmer.setActive = dimmer.functions.setActive()
            }
        }


        ctrl.setDimmable = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.setDimmable ) {
                dimmer.setDimmable = dimmer.functions.setDimmable()
            }
        }


        ctrl.setDimmed = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.setDimmed ) {
                dimmer.setDimmed = dimmer.functions.setDimmed()
            }
        }


        ctrl.setPageDimmer = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.setPageDimmer ) {
                dimmer.setPageDimmer = dimmer.functions.setPageDimmer()
            }
        }


        ctrl.setDisabled = ( dimmer ) => {
            if( dimmer.functions && dimmer.functions.setDisabled ) {
                dimmer.setDisabled = dimmer.functions.setDisabled()
            }
        }


        ctrl.displayDimmer = ( dimmer, display ) => {
            if( display && dimmer ) {
                dimmer.display = display
            }
        }


        ctrl.setDimmerState = ( dimmer, state ) => {
            if( state && dimmer ) {
                dimmer.state = state
            }
        }


        setup();

    }


    angular
    .module('smDimmerRoute')
    .controller( 'DimmerDocsCtrl', DimmerDocsCtrl );


})()