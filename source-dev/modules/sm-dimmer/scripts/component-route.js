

(() => {

    'use strict'

    const smDimmerComponentRoute = {
    	controller: 'DimmerDocsCtrl as Ctrl',
        templateUrl: '../../modules/sm-dimmer/docs.html'
    }


    angular
    .module( 'smDimmerRoute', ['smDimmer'] )
    .component( 'smDimmerComponentRoute', smDimmerComponentRoute )


})()
