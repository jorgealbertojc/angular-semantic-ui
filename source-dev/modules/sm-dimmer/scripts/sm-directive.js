

(() => {
    'use strict'


    const smDimmer = ( ) => {

        return {
            restrict: 'A',
            replace: true,
            transclude: {
                'dimmerPane':'?dimmerPane',
                'dimmerTemplate': '?dimmerTemplate'
            },
            scope:{
                display: "=",
                state: "=",
                functions: "=",
                dimmerClass: "=",
                hover: "="
            },
            template: `
                <div>
                    <div ng-transclude="dimmerPane">  1 </div>
                    <div class="ui dimmer {{dimmerClass}} {{state}}">
                        <div ng-transclude="dimmerTemplate"></div>
                    </div>
                </div>
            `,
            link: ( scope, element, attrs ) => {


                        let dimmer = $(element).find('.dimmer')


                        const setup = () => {
                            setDimmer()
                            if( scope.functions )
                                setFunctions()

                        }


                        const setFunctions = () => {

                            if( scope.functions.setOpacity )
                                scope.functions.setOpacity = ( _value ) => {
                                    $( element ).dimmer( `set opacity(${_value})` )
                                }

                            if( scope.functions.getDimmer )
                                scope.functions.getDimmer = () => {
                                    let dimmerDom = $( element ).dimmer( 'get dimmer' )
                                    return dimmerDom[0] || 'not found'
                                }

                            if( scope.functions.getDuration )
                                scope.functions.getDuration = () => {
                                    let duration = $( element ).dimmer( 'get duration' )
                                    return duration || 'not found'
                                }

                            if( scope.functions.hasDimmer )
                                scope.functions.hasDimmer = () => {
                                    return !!$( element ).dimmer( 'has dimmer' )
                                }

                            if( scope.functions.isActive )
                                scope.functions.isActive = () => {
                                    return !!$( element ).dimmer( 'is active' )
                                }

                            if( scope.functions.isDisabled )
                                scope.functions.isDisabled = () => {
                                    return !!$( element ).dimmer( 'is disabled' )
                                }

                            if( scope.functions.isAnimating )
                                scope.functions.isAnimating = () => {
                                    return !!$( element ).dimmer( 'is animating' )
                                }

                            if( scope.functions.isDimmer )
                                scope.functions.isDimmer = () => {
                                    return !!$( element ).dimmer( 'is dimmer' )
                                }

                            if( scope.functions.isDimmable )
                                scope.functions.isDimmable = () => {
                                    return !!$( element ).dimmer( 'is dimmable' )
                                }

                            if( scope.functions.isPageDimmer )
                                scope.functions.isPageDimmer = () => {
                                    return !!$( element ).dimmer( 'is page dimmer' )
                                }

                            if( scope.functions.setActive )
                                scope.functions.setActive = () => {
                                    return !!$( element ).dimmer( 'set active' )
                                }

                            if( scope.functions.setDimmable )
                                scope.functions.setDimmable = () => {
                                    return !!$( element ).dimmer( 'set dimmable' )
                                }

                            if( scope.functions.setDimmed )
                                scope.functions.setDimmed = () => {
                                    return !!$( element ).dimmer( 'set dimmed' )
                                }

                            if( scope.functions.setPageDimmer )
                                scope.functions.setPageDimmer = () => {
                                    return !!$( element ).dimmer( 'set page dimmer' )
                                }

                            if( scope.functions.setDisabled )
                                scope.functions.setDisabled = () => {
                                    return !!$( element ).dimmer( 'set disabled' )
                                }



                        }


                        const setDimmer = () => {

                            if( !!scope.display ){
                                $( element ).dimmer( scope.display )
                            }

                            if( !!scope.hover ) {
                                $( element ).dimmer({
                                    on: 'hover'
                                })
                            }


                        }


                        scope.changeView = ( state ) => {
                            $( element ).dimmer( state )
                        }


                        scope.$watch( 'display', function( _new, _old ){
                            if( _old !== _new ){
                                scope.changeView( _new )
                            }
                        });


                        setup()

            }
        }

    }


    angular
    .module( 'smDimmer', [] )
    .directive( 'smDimmer', smDimmer )

})()
