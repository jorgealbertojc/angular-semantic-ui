

(() => {
    'use strict'


    const SmDropdownCtrl = function ( ) {

        const ctrl = this


        const setup = () => {
            setupVars()
        }


        const setupVars = () => {

            ctrl.configDropdown1 = {
                on: 'hover'
            }

            ctrl.configDropdown2 = {
                onChange: ( _value, _text, _$choice) => {
                    alert( 'Valor: ' + _value + ', ' + 'Texto: ' + _text)
                }
            }

            ctrl.configDropdown3 = {
                maxSelections: 3
            }

            ctrl.configDropdown4 = {
                maxSelections: 3,
                useLabels: false
            }

            ctrl.configDropdown5 = {
                apiSettings: {
                    url: '//api.semantic-ui.com/tags/{query}'
                }
            }

            ctrl.configDropdown6 = {
                debug: true,
                direction: 'upward',
                transition: 'scale'
            }

            ctrl.behavior3 = {
                input: "get placeholder text",
                output: ""
            }

            ctrl.behavior4 = "show"

        }


        ctrl.clean = function () {
            ctrl.clearDropdown = 'clear'
        }


        ctrl.reset = function () {
            ctrl.resetDropdown = 'restore defaults'
        }


        setup()


    }


    angular
    .module('angularSemanticUI')
    .controller( 'SmDropdownCtrl', SmDropdownCtrl )


})()
