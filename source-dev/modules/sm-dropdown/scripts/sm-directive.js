

(() => {
    'use strict'


    const link = ( $scope, element, attrs, ngModel, transclude ) => {
        let $element = null


        const setup = () => {
            getElementDropdown()
            setOptionsDropdown()
            setBehavior()
            clearResetDropdown()
        }


        const getElementDropdown = () => {
            $element = $(element)
                            .removeClass()
                            .find(".ui.vertical.menu select")

            if( !$element.length ){
                $element = $(element).find(".ui.dropdown")
            }
        }


        const setOptionsDropdown = () => {
            $scope.smConfig = $scope.smConfig || {}

            $element.dropdown( $scope.smConfig )
        }


        const setBehavior = () => {

            if ( angular.isObject( $scope.smBehavior ) ) {
                $scope.smBehavior.output = $element.dropdown( $scope.smBehavior.input )
            } else if ( angular.isString( $scope.smBehavior ) ) {
                $element.dropdown( $scope.smBehavior )
            }

        }


        const clearResetDropdown = () => {
            $scope.$watch('smClearOrReset', function( _newValue, _oldValue ){
                if( angular.equals(_newValue, _oldValue) ||  angular.equals(_newValue, null) ){
                    return
                }
                $element.dropdown( _newValue )
                $scope.smClearOrReset = null
            })
        }


        setup()


    }


    const smDropdown = () => {
        return {
            restrict: 'A',
            replace: true,
            transclude: 'element',
            template: '<ng-transclude></ng-transclude>',
            scope: {
                smConfig: "=?",
                smClearOrReset: "=?",
                smBehavior: "=?"
            },
            link: link
        }
    }


    angular
    .module( 'smDropdown', [] )
    .directive( 'smDropdown', smDropdown )


})()
