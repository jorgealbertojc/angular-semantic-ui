

(() => {

    'use strict'

    const smDropdownComponentRoute = {
        templateUrl: 'modules/sm-dropdown/docs.html'
    }


    angular
    .module( 'smDropdownRoute', ['smDropdown'] )
    .component( 'smDropdownComponentRoute', smDropdownComponentRoute )


})()
