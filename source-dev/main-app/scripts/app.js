

angular
.module( 'angularSemanticUI', [
    'ngComponentRouter',
    'smAccordionRoute',
    'smCheckboxRoute',
    'smDimmerRoute',
    'smDropdownRoute',
    'smEmbedRoute',
    'hljs'
])

.value( '$routerRootComponent', 'rootComponent' )

.component( 'rootComponent', {
    templateUrl:  'main-app/views/root-component.html',
    $routeConfig: [
        { path: '/sm-accordion', name: 'SmAccordion', component: 'smAccordionComponentRoute', useAsDefault: true  },
        { path: '/sm-checkbox', name: 'SmCheckbox', component: 'smCheckboxComponentRoute'},
        { path: '/sm-dimmer', name: 'SmDimmer', component: 'smDimmerComponentRoute' },
        { path: '/sm-dropdown', name: 'SmDropdown', component: 'smDropdownComponentRoute' },
        { path: '/sm-embed', name: 'SmEmbed', component: 'smEmbedComponentRoute' }
    ]
})
