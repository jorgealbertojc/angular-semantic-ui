
# angular-semantic-ui


## Content
1. [Introduction](#introduction)
1. [Scaffolding](#scaffolding)
1. [Styles Guide](#styles-guide)


## Introduction

### External dependencies
    - Gulp
        - Task manager
        - BabelJs
            - Compiles JS with EcmaScript6 to EcmaScript5
        - Gulp-jade
            - Compiles Jade to HTML
        - Gulp-Stylus
            - Compiles Stylus to CSS
    - Unit Testing
        - Karma
            - To run unit tests
        - Browserify (Unit testing)
            - To load modules to the browser when unit testing
    - Javascript
        -   AngularJS

### Requirements
    Get and install:
        > node.js 
        > git
    Commands:
        > npm install
        > bower install

### Gulp Tasks
    - gulp
        - Executes Gulp default tasks and generates files for development
    - gulp production
        - Generates files for production
    - gulp test
        - Starts Karma server and executes the unit tests

### Unit Testing
    - Karma
    - Jasmine

> Folders Tree
```javascript
    |-- tests
        |-- modules
            |-- component-1
                |-- modules
                |-- test.js
            |-- component-2
                |-- modules
                |-- test.js
            ...
            |-- component-n
                |-- modules
                |-- test.js
```

## Scaffolding

```javascript

    |-- _config
        |-- config.js

    |-- build
        |-- index.html
        |-- bower_components
        |-- bower_components
        |-- modules
            |-- sm-module1
                |-- index.html
                |-- sm-module1-component/directive.html
                |-- scripts
                    |-- sm-module1-component-route.js
                    |-- sm-module1-component/directive/service.js
                    |-- sm-module1-ctrl.js
                |-- styles/
                    |-- style.css
                
        |-- main-app
            |-- scripts
                |-- app.js
            |-- styles
            |-- views

    |-- dist
        |-- anguar-semantic-ui.js
        |-- anguar-semantic-ui.min.js
        |-- angular-semantic-ui.css
        |-- angular-semantic-ui.min.css
        |-- modules
            |-- sm-module-1
                |-- sm-component-1.css 
                |-- sm-component-1.min.css
                |-- sm-component-1.js 
                |-- sm-component-1.min.js
                |-- sm-component-1.tpl.js
                |-- sm-component-1.tpl.min.js

    |-- source-dev
        |
        |-- bower_components
        |
        |-- modules
            |-- commons
                |-- styles/
                |-- views/
            |-- sm-module1
                |-- index.jade
                |-- sm-module1-component/directive.jade
                |-- scripts
                    |-- sm-module1-component-route.js
                    |-- sm-module1-component/directive/service.js
                    |-- sm-module1-ctrl.js
                |-- styles/
                    |-- style.styl
            ...
            |-- sm-component-n
        |
        |-- tests
            |-- modules
                |-- sm-module-1
                    |-- sm-component-controllers-1-tests.js
                    |-- sm-component-directive-1-tests.js
                ...
                |-- sm-module-n
        |
        |-- index.jade
        |-- main-app
            |-- scripts
                |-- app.js
            |-- styles
            |-- views
        
```

    
## Styles-guide

The project is built following the styles-guide from https://github.com/ifedu/styleguide-web
> Author: **ifedu** (https://github.com/ifedu)

The styles-guide covers Jade, Stylus, and JS(ES6). 

Besides, there are four patterns used in JS files:

### Breaklines

> - Two breaklines after and before of a function

```javascript
    ...
    ...
    const function1 = () => {
    }
    ...
    ...
    const function2 = () => {
    }
    ...
    ...
    const function3 = () => {
    }
    ...
    ...
```


### Spaces
 
> - One space after and before of the block of parameters of a function

```javascript
    const upperCase = ( parameter1, parameter2 ) => {
        ...
    }
```

> - For no arguments or no parameters used in the declared function, no spaces are needed to appear between the brackets.

```javascript
    const upperCase = () => {
        ...
    }
```
> - When passing an object as an argument of a function, first declare de object and then pass just the reference.

```javascript
   let userInfo = {
        name: 'Raul',
        age: '30'
    }

    myFunction( userInfo )
```


### Descriptive parameters

> - Use parameters names which can easliy describe the parameter itself

```javascript
    const upperCase = ( _personalInfo ) => {
        ...
    }
```
:bulb:**Note**:
>    the underscore is used to difference the argument from internal variables of the function.


