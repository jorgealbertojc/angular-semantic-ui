( ( ) => {

    const config = {};

    config.modules = {
        babel: require('gulp-babel'),
        clean: require('gulp-clean'),
        concat: require('gulp-concat'),
        fs: require('fs'),
        gulp: require('gulp'),
        jade: require( 'gulp-jade' ),
        karmaServer: require('karma').Server,
        path:require('path'),
        release: require('gulp-release-tasks')(require('gulp')),
        reporters: require('reporters'),
        serverLiveReload: require( 'gulp-server-livereload' ),
        stylus: require( 'gulp-stylus' ),
        sync: require( 'gulp-sync' )(require('gulp')),
        uglifyjs: require( 'gulp-uglifyjs' ),
        watch: require( 'gulp-watch' )
    };

    config.foldersPath = {
        build: './build',
        dist: './dist',
        rootPath: config.modules.path.join( __dirname, '../' ),
        source: './source-dev',
        tasks: '../node_modules/gulp-tasks-config'
    };

    config.fn ={
        readFolder: ( folder ) => {
            var PATH = config.modules.path.join( __dirname, folder )
            var FILES = config.modules.fs.readdirSync(PATH)

            FILES.forEach((file) => {
                if( `${file}`.search(".js") > -1 ){
                    require(`${config.foldersPath.tasks}/${file}`)(config)
                }
            });
        }
    }


    module.exports = config;

})();
