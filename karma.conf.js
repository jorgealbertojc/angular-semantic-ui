// Karma configuration
// Generated on Thu Feb 25 2016 11:51:47 GMT-0600 (CST)

module.exports = function(config) {
  config.set({

    basePath: '',

    // list of files / patterns to load in the browser
    files: [
        { pattern: 'source-dev/bower_components/angular/angular.js', included: true, watched: false },
        { pattern: 'source-dev/bower_components/angular-mocks/angular-mocks.js', included: true, watched: false },
        { pattern: 'source-dev/bower_components/jquery/dist/jquery.js', included: true, watched: false },
        { pattern: 'source-dev/bower_components/semantic/dist/semantic.js', included: true, watched: false },
        { pattern: 'source-dev/main-app/**/*.js', included: true, watched: true },
        { pattern: 'source-dev/modules/**/*.js', included: true, watched: true },
        { pattern: 'source-dev/tests/modules/**/*.js', included: true, watched: true }
        // { pattern: 'source-dev/**/*.html', included: true }
    ],


    frameworks: [
        'browserify',
        'jasmine-jquery',
        'jasmine'
    ],


    preprocessors: {
        // 'source-dev/bower_components/jquery/**/jquery.js': [ 'browserify' ],
        // 'source-dev/bower_components/semantic/**/semantic.js': [ 'browserify' ],
        'source-dev/tests/**/**/*.js': [ 'browserify' ]
    },


    webpack: {
        module: {
            loaders: [
                { 
                    test: /\.js$/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['es2015']
                    }
                }
            ]
        },
        watch: true
    },


    webpackServer: {
        noInfo: true
    },
    // browserify: {
    //     debug: true,
    //     transform: [ 'babelify' ]
    // },


    plugins : [
        'karma-browserify',
        'karma-chrome-launcher',
        'karma-phantomjs-launcher',
        'karma-jasmine-jquery',
        'karma-jasmine',
        'karma-webpack'
    ],

    // possible values: 'dots', 'progress'
    reporters: ['progress'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    autoWatch: true,

    //browsers: ['Chrome', 'Firefox', 'Safari', 'PhantomJS', 'Opera', 'IE'],
    browsers: ['Chrome'],

    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // how many browser should be started simultaneous
    concurrency: Infinity


  })
};
